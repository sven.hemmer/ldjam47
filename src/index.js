import Phaser from "phaser";
import { BootScene } from './scenes/boot.js';
import { GameScene } from './scenes/game.js';
import { MenuScene } from './scenes/menu.js';
import { GameOverScene } from './scenes/gameover.js';
import { MenuComponent } from './menuComponent.js';

const ASPECT_RATIO = 16/9;
export const SCREEN_WIDTH = 480;
export const SCREEN_HEIGHT = SCREEN_WIDTH / ASPECT_RATIO;

const config = {
	type: Phaser.WEBGL,
	parent: "phaser-example",
	disableContextMenu: true,
	width: SCREEN_WIDTH,
	height: SCREEN_HEIGHT,
	backgroundColor: '#000000',
	fps: {
		target: 60
	},
	scene: [ BootScene, GameScene, MenuScene, GameOverScene ],
	physics: {
		default: 'arcade',
		arcade: {
			gravity: { y: 0 },
			debug: false // set to true to show bounding boxes
		}
	},
	scale: {
		mode: Phaser.Scale.FIT,
		autoCenter: Phaser.Scale.CENTER_BOTH,
		parent: 'content'
	},
	pixelArt: true // prevents blurring of low-res sprites
};

customElements.define('game-menu', MenuComponent);

const game = new Phaser.Game(config);
