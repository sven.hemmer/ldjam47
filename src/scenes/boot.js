/**
 * Loading / splash screen
 */

import shipSpriteSheet from "../assets/ship.png";
import starSpriteSheet from "../assets/stars.png";
import projectileSpriteSheet from "../assets/projectile.png";
import enemyAsteroidSpriteSheet from "../assets/asteroid.png";
import enemyAlienSpriteSheet from "../assets/alien.png";
import fuelSpriteSheet from "../assets/fuel-second.png";
import boostStripSpriteSheet from "../assets/boost-strip.png";
import fuelBarSpriteSheet from "../assets/fuel-bar.png";
import heartSpriteSheet from "../assets/heart.png";

import laser from "../assets/soundfx/laser.ogg";
import woosh from "../assets/soundfx/sound_wooshTom.ogg";
import wooshFailed from "../assets/soundfx/sound_woosh_stutter_Tomon-alternative.ogg";
import collectFuel from "../assets/soundfx/sound_fuelTom.ogg";
import explRock from "../assets/soundfx/sound_explRock.ogg";
import explCrackle from "../assets/soundfx/sound_explcrackle.ogg";
import explPlayer from "../assets/soundfx/sound_explosionTom.ogg";
import missionStarted from "../assets/soundfx/sound_enginesStarted.ogg";
import missionAccomplished from "../assets/soundfx/sound_missionAccomplished.ogg";
import missionFailed from "../assets/soundfx/sound_missionFailed.ogg";
import menuMusic from "../assets/LoopOrbitalSpaceShooter.ogg";
import menuOkSound from "../assets/soundfx/sound_menuTom.ogg";
import { TRACKS } from "../music";

export class BootScene extends Phaser.Scene {

	constructor () {
		super({ key: 'BootScene' });
	}

	preload () {
		// this.fontsReady = false;
		// this.fontsLoaded = this.fontsLoaded.bind(this);
		this.add.text(100, 100, 'loading...');
		const progressPercentageText = this.add.text(100, 130, `0%`);
		const progressItemText = this.add.text(100, 160, `-`);

		this.load.on('progress', function (value) {
			progressPercentageText.setText(`${Math.round((value * 100) * 100) / 100}%`);
		});
		this.load.on('fileprogress', function (file) {
			progressItemText.setText(`> ${file.key}`);
		});

		// this.load.image('loaderBg', './assets/images/loader-bg.png');
		// this.load.image('loaderBar', './assets/images/loader-bar.png');

		this.load.spritesheet('projectile-spritesheet', projectileSpriteSheet, { frameWidth: 32, frameHeight: 32 });
		this.load.spritesheet('ship-spritesheet', shipSpriteSheet, { frameWidth: 32, frameHeight: 32 });
		this.load.spritesheet('flares', starSpriteSheet, { frameWidth: 16, frameHeight: 16 });
		this.load.spritesheet('enemy-asteroid', enemyAsteroidSpriteSheet, { frameWidth: 32, frameHeight: 32 });
		this.load.spritesheet('enemy-alien', enemyAlienSpriteSheet, { frameWidth: 32, frameHeight: 32 });
		this.load.spritesheet('fuel', fuelSpriteSheet, { frameWidth: 32, frameHeight: 32 });
		this.load.spritesheet('boost-strip', boostStripSpriteSheet, { frameWidth: 160, frameHeight: 32 });
		this.load.spritesheet('fuel-bar-sheet', fuelBarSpriteSheet, { frameWidth: 100, frameHeight: 32 });
		this.load.spritesheet('heart', heartSpriteSheet, { frameWidth: 24, frameHeight: 24 });

		for (const [key, value] of Object.entries(TRACKS)) {
			this.load.audio(key, value);
		}
		this.load.audio('menuMusic', menuMusic);
		this.load.audio('laser', laser);
		this.load.audio('collect-fuel', collectFuel);
		this.load.audio('explRock', explRock);
		this.load.audio('explCrackle', explCrackle);
		this.load.audio('explPlayer', explPlayer);
		this.load.audio('missionStarted', missionStarted);
		this.load.audio('missionAccomplished', missionAccomplished);
		this.load.audio('missionFailed', missionFailed);
		this.load.audio('woosh', woosh);
		this.load.audio('wooshFailed', wooshFailed);
		this.load.audio('menuOkSound', menuOkSound);

		// WebFont.load({
		// 	google: {
		// 		families: ['Bangers']
		// 	},
		// 	active: this.fontsLoaded
		// });
	}

	update () {
		// if (this.fontsReady) {
		this.scene.start('MenuScene');
		// }
	}

	fontsLoaded () {
		// this.fontsReady = true;
	}

}
