import { Player, Ship } from "../player";
import StarField from "../starfield";
import { OrbitalCoords } from "../orbitalCoords";
import { WaveGenerator } from "../waveGenerator";
import { sendStatistics, getStatistics } from "../highscore";
import { MusicPlayer, TRACK_KEYS } from "../music";

import { Hud } from "../hud/hud";

// keep track of position within level
class LoopTimer {

	constructor(loopCallback) {
		this.startTime = NaN;
		this.position = 0;
		this.loopCallback = loopCallback;
		this.callbackCalled = false;
	}

	update(msec, delta) {
		if (Number.isNaN(this.startTime)) {
			this.startTime = msec;
		}

		this.position = (msec - this.startTime) / 1000;

		// calling transition events 1 second before the end of the song
		if (this.position >= 31) {
			this.loopCallback();
			this.position -= 32;
			this.startTime += 32000;
		}
	}

	// returns position between 0 and 32, in seconds
	getLevelPosition() {
		return this.position;
	}

	// start at beginning of loop again.
	reset() {
		this.startTime = NaN;
		this.position = 0;
	}
}

/**
 * Main Game
 */
export class GameScene extends Phaser.Scene {

	constructor () {
		super({ key: 'GameScene' });

		this.enemies = new Set();
		this.items = new Set();
		this.sounds = {};
		this.musicPlayer = new MusicPlayer(this);
	}

	create() {
		this.musicPlayer.init();

		this.sounds['laser'] = this.game.sound.add('laser', 1, false);
		this.sounds['woosh'] = this.game.sound.add('woosh', { volume: 2.5 });
		this.sounds['wooshFailed'] = this.game.sound.add('wooshFailed', { volume: 2.5 });
		this.sounds['collect-fuel'] = this.game.sound.add('collect-fuel');
		this.sounds['explRock'] = this.game.sound.add('explRock');
		this.sounds['explCrackle'] = this.game.sound.add('explCrackle');
		this.sounds['explPlayer'] = this.game.sound.add('explPlayer');
		this.sounds['missionStarted'] = this.game.sound.add('missionStarted');
		this.sounds['missionAccomplished'] = this.game.sound.add('missionAccomplished');
		this.sounds['missionFailed'] = this.game.sound.add('missionFailed');

		this.orbitalCoords = new OrbitalCoords();
		this.starField = new StarField(this, this.orbitalCoords);

		this.addReusableAnimations();

		this.initGame()
		this.hud = new Hud(this, this.player);

		registerSpaceBarToSendDummyStatistics(this);
	}

	// initialize new game, starting at level 0, with 5 fresh lives
	initGame() {
		this.playEffect('missionStarted');
		const text = this.add.text(
			100, 150, "Collect fuel to reach next orbit"
		);
		setTimeout(() => text.destroy(), 3000);

		this.player = new Player();
		this.ship = this.add.existing(new Ship(this, this.player));
		this.currentLevel = 0;
		this.musicPlayer.play(TRACK_KEYS[this.currentLevel % TRACK_KEYS.length]);
		this.initLevel();
	}

	// initialize level, re-using current player object.
	initLevel() {
		this.loopTimer = new LoopTimer(() => this.onLevelLoop());
		this.orbitalCoords.level = this.currentLevel + 1;
		this.waveGenerator = new WaveGenerator(this, this.currentLevel, this.loopTimer);
		this.maxFuel = this.waveGenerator.maxFuel;
	}

	// called just before the end of every 32 second loop (at around 30 seconds)
	onLevelLoop() {
		if (this.ship.exploded) { return; }

		addBoostStrips(this);
		let message = null;
		if (this.player.collectedFuel >= this.maxFuel) {
			message = "Jumping to higher orbit!"
			this.player.collectedFuel = 0;
			{// transition effect
				this.playEffect('woosh');
				// this.starField.ridiculousSpeed();
				// setTimeout(() => {
				// 	// reset transition effect
				// 	this.starField.normalSpeed();
				// },
				// 3800);
				this.tweens.add({
					targets: this.orbitalCoords,
					level: { from: this.currentLevel + 1, to: this.currentLevel + 2 },
					ease: 'Linear',
					duration: 3800,
					repeat: 0,
				})
				this.tweens.add({
					targets: this.starField,
					speedFactor: { from: 1, to: 4 },
					ease: 'Linear',
					duration: 1700,
					repeat: 0,
					yoyo: true,
				})
			}
			this.nextLevel();
		}
		else {
			this.playEffect('wooshFailed');
			this.player.collectedFuel = 0;
			this.waveGenerator.reset();
			message = "Not enough fuel, loop around!"
			// TODO - failed to transition
		}

		const text = this.add.text(
			100, 150, message
		);
		setTimeout(() => text.destroy(), 3000);
	}

	// called on player death
	onPlayerDeath() {
		for (const e of this.enemies) {
			e.destroy();
		}
		this.enemies = new Set();
		for (const i of this.items) {
			i.destroy();
		}
		this.items = new Set();

		// reset player state
		this.ship = this.add.existing(new Ship(this, this.player));
		this.player.collectedFuel = 0;

		this.musicPlayer.play(TRACK_KEYS[this.currentLevel % TRACK_KEYS.length]);

		if (this.player.lives <= 0) {
			this.playEffect('missionFailed');
			this.musicPlayer.stop();
			this.scene.start('GameOverScene');
		}
		else {
			this.initLevel(); // also resets music
		}
	}

	nextLevel() {
		this.currentLevel += 1;
		this.player.addToScore(10);
		this.musicPlayer.enqueue(TRACK_KEYS[this.currentLevel % TRACK_KEYS.length]);
		this.initLevel();
	}

	addEnemy(enemy) {
		this.enemies.add(enemy);
	}

	removeEnemy(enemy) {
		this.enemies.delete(enemy);
	}

	addItem(item) {
		this.items.add(item);
	}

	removeItem(item) {
		this.items.delete(item);
	}

	checkCollisions(entity, group, entityCallback, groupItemCallback) {
		for (const groupItem of group) {
			if (!groupItem.exploded && this.physics.collide(entity.sprite, groupItem.sprite)) {
				groupItemCallback(groupItem);
				entityCallback(entity, groupItem);
			}
		}
	}

	playEffect(name) {
		const sound = this.sounds[name];
		if (sound) {
			sound.play();
		}
	}

	update(...args) {
		this.loopTimer.update(...args);
		this.waveGenerator.update(...args);
		this.hud.update({ ...args, maxFuel: this.maxFuel });
	}

	addReusableAnimations() {
		this.anims.create({
			key: 'asteroid-normal',
			frames: this.anims.generateFrameNames('enemy-asteroid', { start: 0, end: 8 }),
			frameRate: 5,
			repeat: -1
		});
		this.anims.create({
			key: 'asteroid-explosion',
			frames: this.anims.generateFrameNames('enemy-asteroid', { start: 9, end: 16 }),
			frameRate: 5,
			repeat: 0
		});

		this.anims.create({
			key: 'alien-normal',
			frames: this.anims.generateFrameNames('enemy-alien', { start: 0, end: 7 }),
			frameRate: 3,
			repeat: -1
		});
		this.anims.create({
			key: 'alien-explosion',
			frames: this.anims.generateFrameNames('enemy-alien', { start: 9, end: 16 }),
			frameRate: 3,
			repeat: 0
		});
		this.anims.create({
			key: 'fuel-normal',
			frames: this.anims.generateFrameNames('fuel', { start: 0, end: 8 }),
			frameRate: 5,
			repeat: -1
		});
		this.anims.create({
			key: 'projectile-normal',
			frames: this.anims.generateFrameNames('projectile-spritesheet', { start: 0, end: 3 }),
			frameRate: 5,
			repeat: -1
		});
		this.anims.create({
			key: 'ship-normal',
			frames: this.anims.generateFrameNames('ship-spritesheet', { start: 0, end: 5 }),
			frameRate: 5,
			repeat: -1
		});
		this.anims.create({
			key: 'ship-dark',
			frames: this.anims.generateFrameNames('ship-spritesheet', { start: 6, end: 11 }),
			frameRate: 5,
			repeat: -1
		});
		this.anims.create({
			key: 'ship-explode',
			frames: this.anims.generateFrameNames('ship-spritesheet', { start: 12, end: 17 }),
			frameRate: 8,
			repeat: 0
		});
		this.anims.create({
			key: 'boost-strip-normal',
			frames: this.anims.generateFrameNames('boost-strip', { start: 0, end: 5 }),
			frameRate: 9,
			repeat: -1
		});

	}
}

function addBoostStrips(scene) {
	const top = 15;

	const START_POS = 440;

	const boostStripPositions = [
		{ x: START_POS, y: top },
		{ x: START_POS, y: top + 40 },
		{ x: START_POS, y: top + 80 },
		{ x: START_POS, y: top + 120 },
		{ x: START_POS, y: top + 160 },
		{ x: START_POS, y: top + 200 },
		{ x: START_POS, y: top + 240 },
	]
	const sprites = [];
	boostStripPositions.forEach(pos => {
		const sprite = scene.physics.add.sprite(pos.x, pos.y, 'boost-strip');
		sprite.anims.play('boost-strip-normal', true);
		sprite.setVelocityX(-200);
		sprites.push(sprite);
	})

	setTimeout(
		() => {
			sprites.forEach(sprite => sprite.destroy());
		},
		3000
	);
}

function registerSpaceBarToSendDummyStatistics(scene) {
	const spaceKey = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
	spaceKey.on('down', async function (key, event) {
		await sendStatistics({ username: "Bob", score: 99 });
		console.log(await getStatistics());
	});
}
