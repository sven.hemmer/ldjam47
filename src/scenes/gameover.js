
export class GameOverScene extends Phaser.Scene {

	constructor () {
		super({ key: 'GameOverScene' });
	}

	create () {
		console.log("Game Over")
		this.add.text(150, 150, 'GAME OVER');

		this.time.addEvent({
			delay: 2000,
			callback: () => this.scene.start('MenuScene'),
			callbackScope: this,
			loop: false
		});
	}

}
