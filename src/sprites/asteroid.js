import Phaser from 'phaser';

export class Asteroid extends Phaser.GameObjects.GameObject {
	constructor(scene, start, velocity) {
		super(scene);
		this.start = start;
		//TODO correctly compute from start-values
		//this.t = Math.PI / 4;
		//this.r = start.y / SCREEN_HEIGHT;

		this.orbitalCoords = scene.orbitalCoords;

		const { t, r } = this.orbitalCoords.fromScreenCoords(start.x, start.y);
		this.t = t;
		this.r = r;

		this.scene.addEnemy(this);
		this.sprite = this.scene.physics.add.sprite(this.start.x, this.start.y, 'enemy-asteroid');
		this.sprite.play('asteroid-normal', true);
		Object.assign(this.sprite.body.offset, { x: 8, y: 8 });
		Object.assign(this.sprite.body, { height: 16, width: 16 });
	}

	preUpdate(time, delta) {
		const { x, y } = this.orbitalCoords.convertToScreen(this.t, this.r);
		this.sprite.setPosition(x, y);

		this.t += this.angularSpeed;

		if (x < -40) {
			this.removeAndDestroy();
		}
	}

	explode() {
		if (this.exploded) {
			return;
		}
		this.exploded = true;
		this.scene.playEffect('explRock');
		this.angularSpeed = 0;
		// this.sprite.body.setVelocity(0, 0);
		this.sprite.body.setAcceleration(0, 0);
		this.sprite.play('asteroid-explosion', false);
		this.sprite.once(Phaser.Animations.Events.SPRITE_ANIMATION_COMPLETE, () => {
			this.removeAndDestroy();
		});
	}

	removeAndDestroy() {
		this.scene.removeEnemy(this);
		this.destroy();
	}

	destroy() {
		this.sprite.destroy();
		super.destroy();
	}

}
