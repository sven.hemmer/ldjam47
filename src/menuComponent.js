import logo from "./assets/logo.png";

export class MenuComponent extends HTMLElement {

	constructor() {
		super();
		this.attachShadow({ mode: 'open' });
		this.shadowRoot.innerHTML = `
	<style>
		:host {
			display: flex;
			justify-content: center;
			align-items: center;
			height: 100vh;

			background-image: url(${logo});
			background-position: center;
			background-repeat: no-repeat;
			background-size: contain;
		}

		button {
			background-color: #4C50AF; /* Blue */
			border: none;
			color: white;
			text-align: center;
			text-decoration: none;
			display: inline-block;
			font-size: 16px;
			margin: 2rem;
			width: 10rem;
			height: 4rem;
		}
		button:hover {
			background-color: #6C70DF; /* Blue */
		}
	</style>

	<div class="main">
		<div class="buttonBar">
			<button id="startGame">Start Game</button>
		</div>
	</div>
`;
	}

	connectedCallback() {
		this.shadowRoot.querySelector("#startGame").addEventListener("click", (event) => {
			this.dispatchEvent(new CustomEvent("Start"));
			this.dispatchEvent(new CustomEvent("button-pressed"));
		});
	}

}
