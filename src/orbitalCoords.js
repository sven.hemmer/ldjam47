/*
Convert orbital coordinates to screen coordinates.
Orbit is defined as a broad ring shape (with radius between ringBase and ringBase + ringWidth)
around a central point (xofst, yofst)
*/
export class OrbitalCoords {

	constructor() {
		this.level = 1;
	}

	set level(value) {
		this._level = Math.max (1, value); // basis for level progression
		const margin = 50;
		const screenH = 300;

		this.ringBase = 400 + (50 * this._level); // base radius of orbit
		this.ringWidth = screenH + (2 * margin); // screen height plus some margin

		this.xofst = 0;
		this.yofst = this.ringBase + screenH;
	}

	/**
	 *
	 * @param {*} t angle, between 0 and 2*PI
	 * @param {*} r perpendicular coordinate, between 0 (near inside ring) and 1 (near outside ring)
	 */
	convertToScreen(t, r) {
		const absR = (r * this.ringWidth) + this.ringBase;
		const x = (absR * Math.cos(-t)) + this.xofst;
		const y = (absR * Math.sin(-t)) + this.yofst;
		return { x, y };
	}

	fromScreenCoords(x, y) {
		const worldX = x - this.xofst;
		const worldY = y - this.yofst;
		const absR = Math.sqrt(worldX * worldX + worldY * worldY);
		const t = -Math.atan2(worldY, worldX);
		const r = (absR - this.ringBase) / this.ringWidth;
		return { r, t };
	}

	testOneConversion(x, y) {
		const { t, r } = this.fromScreenCoords(x, y);
		console.log(`Convert x,y [${x},${y}] to t,r [${t},${r}]`);
		const p = this.convertToScreen(t, r);
		console.log(`Convert back to [${p.x},${p.y}]`);
	}

	test() {
		this.testOneConversion(0, 0);
		this.testOneConversion(0, 300);
		this.testOneConversion(480, 0);
		this.testOneConversion(480, 300);
	}
}
