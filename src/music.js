import track1 from "./assets/SaturnSymphonie_Lvl_1.ogg";
import track2 from "./assets/SaturnSymphonie_Lvl_2.ogg";
import track3 from "./assets/SaturnSymphonie_Lvl_3.ogg";
import track4 from "./assets/SaturnSymphonie_Lvl_4.ogg";
import track5 from "./assets/SaturnSymphonie_Lvl_5.ogg";
import track6 from "./assets/SaturnSymphonie_Lvl_6.ogg";
import track7 from "./assets/SaturnSymphonie_Lvl_7.ogg";
import track8 from "./assets/SaturnSymphonie_Lvl_8.ogg";
import track9 from "./assets/SaturnSymphonie_Lvl_9.ogg";

const VOLUME = 0.5;

export const TRACKS = {
	track1, track2, track3, track4, track5, track6, track7, track8, track9
}
export const TRACK_KEYS = Object.keys(TRACKS);

export class MusicPlayer {

	constructor(game) {
		this.game = game;
		this.currentTrack = null;
		this.musicQ = [];
		this.tracks = {};
	}

	init() {
		for (const key of Object.keys(TRACKS)) {
			this.tracks[key] = this.game.sound.add(key, VOLUME, true);
		}
	}

	enqueue(track) {
		if (this.game_music) {
			this.musicQ.push(track);
		}
		else {
			this.play(track)
		}
	}
	play(track) {
		this.musicQ = [];
		this.playImmediate(track);
	}

	playImmediate(track) {
		console.log("Starting track: ", track);
		this.stop();
		this.currentTrack = track;
		this.game_music = this.tracks[track];
		this.game_music.on('looped', () => this.onLoop());
		this.game_music.volume = VOLUME;
		this.game_music.play( { loop: true });
	}

	fadeOut(scene) {
		if (this.game_music) {
			scene.tweens.add({
				targets:  this.game_music,
				volume:   0,
				duration: 1000
			});
		}
		
	}

	onLoop() {
		console.log ("Loop");
		if (this.musicQ.length > 0) {
			let track = this.musicQ.shift();
			this.playImmediate(track);
		}
	}

	stop() {
		if (this.game_music) {
			this.game_music.stop();
			this.game_music.off('looped');
			this.game_music = null;
		}
	}

}