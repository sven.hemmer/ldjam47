/*
Define waves of objects
*/

// test-level
// 0: [
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f',
// 	's...sf...f'
// ],

import { Asteroid, Alien } from "./sprites";
import { Fuel } from "./sprites/fuel";

const LOOP_LENGTH = 30;

const LEVELS = {
	0: [
		'..........',
		'..........',
		'..........',
		'...s......',
		'.....b....',
		'.......s..',
		'....f.....',
		'.s........',
		'..........',
		'.s........',
		'...s......',
		'..........',
		'..........',
		'....f.....',
		'..........',
		'......s...',
		'..........',
		'...s......',
		'..........',
		'........s.',
		'..........',
		'..........',
		'..........',
		'.....b....',
		'..........',
		'...s......',
		'.......s..',
		'..........',
		'.s........',
		'..........'
	],
	1: [
		'..........',
		'..........',
		'..........',
		'...s......',
		'.....b....',
		'.......s..',
		'....s.....',
		'.s........',
		'....s.....',
		'.s........',
		'...sf.....',
		'..........',
		'..........',
		'.b........',
		'..........',
		'......s...',
		'.....f..b.',
		'..........',
		'..........',
		'........s.',
		'..........',
		'.....s....',
		'......f...',
		'.....b....',
		'..........',
		'...s......',
		'..........',
		'..........',
		'.s........',
		'..........'
	],
	2: [
		'..........',
		'..........',
		'..........',
		'...s......',
		'.....b....',
		'.......s..',
		'....s.....',
		'.s........',
		'....s.....',
		'.s........',
		'...sf.....',
		'..........',
		'..........',
		'.b........',
		'..........',
		'......s...',
		'.....f..b.',
		'..........',
		'..........',
		'........s.',
		'..........',
		'.....s....',
		'..........',
		'.....a....',
		'..........',
		'...s......',
		'......f...',
		'..........',
		'.s........',
		'..........'
	],
	3: [
		'..........',
		'..........',
		'..........',
		'...s.s....',
		'.....b....',
		'..s....s..',
		'....s.....',
		'.s........',
		'....s.....',
		'.s........',
		'..a.s.....',
		'..........',
		'........f.',
		'.b........',
		'..........',
		'......s...',
		'.....a..b.',
		'..........',
		'..........',
		'..f.....s.',
		'..........',
		'.....s....',
		'..........',
		'.....a....',
		'..........',
		'.s.s..f...',
		'..s.......',
		'..........',
		'.s....s...',
		'..........'
	],
	4: [
		'..........',
		'..........',
		'....b.....',
		'...s.s....',
		'.....b....',
		'..s....s..',
		'....sf...s',
		'.s........',
		'....s.....',
		'.s........',
		'..b.s.....',
		'.......f..',
		'....b.....',
		'.b........',
		'..........',
		'......s...',
		'.....b..b.',
		'..........',
		'..........',
		'..b.....s.',
		'..........',
		'.....s....',
		'.....f....',
		'.....a....',
		'..........',
		'.s.s..b...',
		'..s.......',
		'..........',
		'.s....s...',
		'..........'
	],
	5: [
		'..........',
		'.s......s.',
		'....b.....',
		'...s.s....',
		'.....b....',
		'..s....s..',
		'....s....s',
		'.s........',
		'....s.....',
		'.s...f....',
		'..a.s.....',
		'.......b..',
		'....a.....',
		'.b........',
		'..........',
		'......s...',
		'.....b..b.',
		'......f...',
		'..........',
		'..b.....s.',
		'..........',
		'.....s....',
		'...f.b....',
		'.....b....',
		'..........',
		'.s.s..b...',
		'..s.......',
		'..........',
		'.s....s...',
		'..........'
	],
	6: [
		's........s',
		'.s......s.',
		'....b..s..',
		'...s.s....',
		'.....bf...',
		'..s....s..',
		'....s....s',
		'.s........',
		'....s.....',
		'.s........',
		'..a.s.....',
		'.......b..',
		'....b..s..',
		'.a........',
		'..........',
		'......s...',
		'.....b..b.',
		'......b...',
		'....f.....',
		'..b.....s.',
		'..........',
		'.....s....',
		'.b...b....',
		'.....b....',
		'...f......',
		'.s.s..b...',
		'..s.......',
		'..........',
		'.s....s...',
		'..........'
	],
	7: [
		's........s',
		'.s......s.',
		'....a..s..',
		'.b.s.s....',
		'....fb....',
		'..s....s..',
		'....s....s',
		'.s........',
		'....s.....',
		'.s........',
		'..b.s.....',
		'....f..a..',
		'....b..s..',
		'.a........',
		'...bs.....',
		'......s...',
		'.....a..b.',
		'......b...',
		'....b.....',
		'..b.....s.',
		'...f......',
		'.....s....',
		'.b...b....',
		'.....bf...',
		'.b.....b..',
		'.s.s..a...',
		'..s.......',
		'..........',
		'.s....s...',
		'..........'
	],
	8: [
		's........s',
		'.s......s.',
		'...bb..s..',
		'.b.sas....',
		'.....b....',
		'..s..f.s..',
		'....s....s',
		'.s........',
		'....s.....',
		'.s........',
		'..a.s.....',
		'b....b.f.b',
		'.......s..',
		'.b.f......',
		'...bs.....',
		'......s...',
		'.....a..b.',
		'a.....b...',
		'....b....b',
		'..a..f..s.',
		'...b...b..',
		'.....s....',
		'.b....b...',
		'.....a....',
		'.s...f...b',
		'.s.s......',
		'..s.......',
		'...b...s..',
		'.s....a...',
		'..........'
	],
}

export class WaveGenerator {

	constructor(scene, currentLevel, loopTimer) {
		this.scene = scene;
		this.currentLevel = currentLevel;
		const leveldata = this.parseLevel();
		const data = leveldata[currentLevel % leveldata.length];
		this.waveData = data.waveData;
		this.maxFuel = data.maxFuel;
		this.pos = 0;
		this.nextTime = 0;
		this.loopTimer = loopTimer;
	}

	parseLevel() {
		const levelContainer = [];
		for (let key of Object.keys(LEVELS)) {
			const level = LEVELS[key];
			const timeRow = level.length/LOOP_LENGTH;
			let maxFuel = 0;
			const waveData = [];
			let time = -1;
			for (let slot of level) {
				time += timeRow;
				const row = slot.split('');
				for (let index in row) {
					if (slot[index] === '.') {
						continue;
					}
					if (slot[index] === 'f') {
						maxFuel++;
					}
					waveData.push({
						time,
						type: slot[index],
						y: index/row.length
					});
				}
			}
			levelContainer.push({ waveData, maxFuel });
		}
		return levelContainer;
	}

	update(msec, delta) {
		while (this.loopTimer.getLevelPosition() > this.nextTime) {
			const data = this.waveData[this.pos];
			this.generateEnemy(data);
			this.pos++;
			if (this.pos >= this.waveData.length) {
				this.pos = 0;
				this.nextTime = 9999; // We're not advancing until reset is called.
				break;
			}
			this.nextTime = this.waveData[this.pos].time;
		}
	}

	reset() {
		this.pos = 0;
		this.nextTime = this.waveData[this.pos].time;
	}

	generateEnemy(data) {
		/* map position between 0 and 1 to an y-offset */
		const orbitalYOffset = (frac) => {
			return (300 * frac) + 100;
		}
		switch(data.type) {
			case 's': {
				const entity = new Asteroid(this.scene, { x: 500, y: orbitalYOffset(data.y) });
				this.scene.add.existing(entity);
				entity.angularSpeed = -160/-3e4;
				// entity.sprite.setVelocityX(-160);
				break;
			}
			case 'b': {
				const entity = new Alien(this.scene, { x: 500, y: 300 * data.y }, 'up-wait-down-wait');
				this.scene.add.existing(entity);
				entity.sprite.setVelocityX(-160);
				break;
			}
			case 'a': {
				const entity = new Alien(this.scene, { x: 500, y: 300 * data.y }, 'circles');
				this.scene.add.existing(entity);
				entity.sprite.setVelocityX(-160);
				break;
			}
			case 'f': {
				const entity = new Fuel(this.scene, { x: 500, y: orbitalYOffset(data.y) });
				this.scene.add.existing(entity);
				entity.angularSpeed = -160/-3e4;
				//entity.sprite.setVelocityX(-160);
				break;
			}
		}
	}
}
