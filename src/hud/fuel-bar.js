import { SCREEN_HEIGHT, SCREEN_WIDTH } from "../index";

const AMOUNT_OF_FRAMES_OF_BAR = 11;

export class FuelBar {
	constructor({ scene, x, y }) {
		this.sprite = scene.add.sprite(x, y, 'fuel-bar-sheet');
		this.sprite.setFrame(0);
	}

	render({ time, delta, player, maxFuel }) {
		const fractionOfBar = Math.min(player.collectedFuel / maxFuel, 1);
		const frame = Math.floor(fractionOfBar * (AMOUNT_OF_FRAMES_OF_BAR - 1));
		this.sprite.setFrame(frame);
	}
}
