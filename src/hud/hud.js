import { SCREEN_HEIGHT, SCREEN_WIDTH } from "../index";
import { FuelBar } from "./fuel-bar";
import { HealthBar } from "./health-bar";

export class Hud {
	constructor(scene, player) {
		this.player = player;
		this.scene = scene;

		this.fuelBar = new FuelBar({
			scene,
			x: SCREEN_WIDTH - 57,
			y: SCREEN_HEIGHT - 20,
		});
		this.healthBar = new HealthBar({
			scene,
			x: SCREEN_WIDTH - 20,
			y: SCREEN_HEIGHT - 55,
		});

		this.scoreText = scene.add.text(
			SCREEN_WIDTH - 200,
			SCREEN_HEIGHT - 20,
			"Score: -"
		);
		this.fuelText = scene.add.text(
			SCREEN_WIDTH - 200,
			SCREEN_HEIGHT - 40,
			"Fuel: -/-"
		);
		this.levelText = scene.add.text(
			20,
			SCREEN_HEIGHT - 20,
			"Orbit: -"
		);

		// this.timeText = scene.add.text(
		// 	SCREEN_WIDTH - 100,
		// 	SCREEN_HEIGHT - 80,
		// 	"Time: -"
		// );
	}

	update({ time, delta, maxFuel }) {
		this.scoreText.setText(`Score: ${this.player.score}`);
		this.levelText.setText(`Orbit: ${this.scene.currentLevel + 1}`);
		this.fuelText.setText(`Fuel: ${this.player.collectedFuel}/${maxFuel}`);
		// this.timeText.setText(`Time: ${this.scene.loopTimer.getLevelPosition()}`);
		this.fuelBar.render({ player: this.player, maxFuel });
		this.healthBar.render({ player: this.player });
	}
}
