/*
Player class
Control ship movement
*/

import Phaser from 'phaser';

import { Projectile } from './projectile.js';
import { MAX_LIVES } from './constants.js';

const SHOOT_INTERVAL = 1e3;
const PLAYER_SPEED = 200;

/** Player stats, kept from one life to the next */
export class Player {
	
	constructor() {
		this.score = 0;
		this.lives = MAX_LIVES;
		this.collectedFuel = 0;
	}

	addToScore(value) {
		const scoreAfterUpdate = this.score + value;
		const didWeCrossAHundrederLine = Math.floor(scoreAfterUpdate/100) - Math.floor(this.score/100);
		if (didWeCrossAHundrederLine) {
			this.lives = Math.min(this.lives + 1, MAX_LIVES);
		}
		this.score = scoreAfterUpdate;
	}

}

/** Ship - object destroyed when player is hit */
export class Ship extends Phaser.GameObjects.GameObject {
	constructor(scene, playerStats) {
		super(scene, 'player');
		this.counter = 0;
		this.stats = playerStats;

		this.sprite = this.scene.physics.add.sprite(100, 150, 'ship-spritesheet');
		this.sprite.anims.play('ship-normal', true);

		this.cursors = this.scene.input.keyboard.createCursorKeys();
		this.width = this.scene.game.config.width;
		this.height = this.scene.game.config.height;
	}

	preUpdate(time, delta) {

		if (this.exploded) {
			return;
		}

		this.counter += delta;
		if (this.counter / SHOOT_INTERVAL >= 1) {
			this.counter %= SHOOT_INTERVAL;
			this.scene.add.existing(new Projectile(this.scene, this.sprite.getRightCenter()));
		}

		const position = this.sprite.getCenter();
		const direction = { x: 0, y: 0 };
		if (this.cursors.left.isDown && position.x > 0) { direction.x = -1; }
		else if (this.cursors.right.isDown && position.x < this.width) { direction.x = 1; }
		if (this.cursors.up.isDown && position.y > 0) { direction.y = -1; }
		else if (this.cursors.down.isDown && position.y < this.height) { direction.y = 1; }

		const normalizedDirection = normalize2dVector(direction);
		this.sprite.setVelocityX(PLAYER_SPEED * normalizedDirection.x);
		this.sprite.setVelocityY(PLAYER_SPEED * normalizedDirection.y);

		this.scene.checkCollisions(this, this.scene.items,
			() => this.onCollectedFuel(), item => item.destroy()
		);

		this.scene.checkCollisions(this, this.scene.enemies,
			() => this.onHitEnemy(), enemy => enemy.destroy()
		);

	}

	onCollectedFuel() {
		this.scene.playEffect('collect-fuel');
		this.stats.collectedFuel += 1;
		this.stats.addToScore(3);
		if (this.stats.collectedFuel === this.scene.maxFuel) {
			this.scene.playEffect('missionAccomplished');
		}
	}

	onHitEnemy() {
		this.exploded = true;
		this.scene.playEffect('explPlayer');

		this.sprite.anims.play('ship-explode', true);
		this.sprite.once(Phaser.Animations.Events.SPRITE_ANIMATION_COMPLETE, () => {
			this.destroy();
		});

		this.scene.musicPlayer.fadeOut(this.scene);

		const scene = this.scene; // defensive copies while object will be destroyed
		const stats = this.stats;
		this.scene.time.addEvent({
			delay: 2500,
			callback: () => {
				stats.lives--;
				scene.onPlayerDeath();
			},
			callbackScope: this,
			loop: false
		});

	}

	destroy() {
		this.sprite.destroy();
		super.destroy();
	}

}

function normalize2dVector(vector) {
	if (vector.x === 0 && vector.y === 0) return vector;
	const norm = Math.sqrt(Math.pow(vector.x, 2) + Math.pow(vector.y, 2));
	return {
		x: vector.x / norm,
		y: vector.y / norm,
	};
}
