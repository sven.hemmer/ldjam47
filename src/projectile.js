import Phaser from 'phaser';
import { Asteroid, Alien } from './sprites';

export class Projectile extends Phaser.GameObjects.GameObject {
	constructor(scene, start) {
		super(scene, 'projectile');
		this.scene = scene;
		this.start = start;


		this.sprite = this.scene.physics.add.sprite(this.start.x, this.start.y, 'projectile-spritesheet');
		this.scene.playEffect('laser');
		Object.assign(this.sprite.body.offset, { x: 8, y: 14 });
		Object.assign(this.sprite.body, { height: 2, width: 14 });
		this.sprite.setVelocity(100, 0);
		this.sprite.setAcceleration(400, 0);
		this.sprite.play('projectile-normal', true);
	}

	preUpdate(time, delta) {
		this.scene.checkCollisions(this, this.scene.enemies,
			(_, enemy) => this.onHitEnemy(enemy), enemy => enemy.explode()
		);

		// TODO: call this.destroy() when the projectile goes out of bounds
	}

	onHitEnemy(enemy) {
		if (enemy instanceof Asteroid) {
			this.scene.player.addToScore(1);
		}
		else if (enemy instanceof Alien) {
			this.scene.player.addToScore(2);
		}
		this.destroy();
	}

	destroy() {
		this.sprite.destroy();
		super.destroy();
	}
}
