# L.O.S.S. - Looped Orbital Space Shooter

![Logo](./src/assets/logo.png)

You’re stuck in an orbit around Saturn! Collect fuel and jump to a higher orbit, to escape!

A Ludum Dare 47 game, by:

* Simon Lenz
* Sven Hemmer
* Amarillion
* ReadyPlayerTom
* .... with help from Dónall O'Donoghue and Jesse van Assen

Play the game online at https://sven.hemmer.gitlab.io/ldjam47/.  
Game Jam page: https://ldjam.com/events/ludum-dare/47/l-o-s-s-looped-orbital-space-shooter

## How to play

Use the cursor keys to move your spaceship. Avoid asteroids and aliens, but collect fuel tanks.

Your ship can defend itself with its forward laser. Unfortunately, the controls of the laser are stuck in a loop, firing automatically every second. You can only control the position of your ship.

Each orbit takes exactly 32 seconds. Within this period, you have to collect the required fuel tanks in order to jump to a higher orbit. If the fuel gage isn't full at the end of
an orbit, your jump fails and you will remain stuck in a loop.

Each orbit has its own music loop. At higher orbits, things move a lot faster so beware! 

## Stuck in a loop

The theme of Ludum Dare 47 was: "Stuck in a loop"

This theme made us think of a space ship stuck in an orbit, looping around a planet forever. The ship can only break out by collecting fuel, reaching a higher orbit. But the ship can never fully escape the planet's gravity.

The theme also made us think of a music loop. Each orbit has its own musical loop associated with it. By reaching higher orbits, you get more varied and interesting music.

Finally, your laser is stuck in a loop and cannot be controlled.

## Discarded game names

- Saturn Space Symphony
- Space Jam
- Rings a bell
- Circling Saturn

## Building & running from scratch

The source code for the game is at https://gitlab.com/sven.hemmer/ldjam47
The game is based on the Phaser 3 game engine.

### Requirements

[Node.js](https://nodejs.org) is required to install dependencies and run scripts via `npm`.

### Available Commands

| Command | Description |
|---------|-------------|
| `npm install` | Install project dependencies |
| `npm start` | Build project and open web server running project |
| `npm run build` | Builds code bundle with production settings (minification, uglification, etc..) |
